<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return view('page.index');
    }


    public function data_tables(){
        return view('table.data-tables');
    }
}
