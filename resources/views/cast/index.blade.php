@extends('layout.master')

@section('title')
    Cast list
@endsection

@section('content')
    <a href="cast/create" class="btn btn-success mb-2">Tambahkan Cast</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <th>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="POST">
                            <a href="/cast/{{ $item->id }}" class="btn btn-info mb-2">Detail</a>
                            <a href="/cast/{{ $item->id }}/edit " class="btn btn-secondary mb-2">Edit</a>
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <h3> Data Not Found</h3>
            @endforelse
        </tbody>
    </table>
@endsection
