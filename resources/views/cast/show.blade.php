@extends('layout.master')

@section('title')
    Cast Detail
@endsection

@section('content')
    <h1>Name: {{ $cast->nama }}</h1>
    <p>Age: {{ $cast->umur }}</p>
    <p>Bio: {{ $cast->bio }}</p>
@endsection
