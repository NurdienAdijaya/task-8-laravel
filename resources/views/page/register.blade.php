@extends('layout.master')

@section('title')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <form action="/welcome" method="post">
        @csrf
      <h3>Sign Up Form</h3>
      <label for="">First name :</label> <br />
      <br />
      <input type="text" name="firstname" placeholder="First name" /> <br />
      <br />
      <label for="">Last name :</label> <br />
      <br />
      <input type="text" name="lastname" placeholder="Last name" /> <br />
      <br />
      <label for="">Gender</label> <br />
      <br />
      <input type="radio" name="gender" value="Male">Male</input><br />
      <input type="radio" name="gender" value="Female">Female</input><br />
      <br />
      <label for="">Nationality</label> <br />
      <br />
      <select name="nationality">
          <option value="indonesia">Indonesia</option>
          <option value="malaysia">Malaysia</option>
          <option value="singapore">Singapore</option>
          <option value="brunei">Brunei</option>
      </select><br />
      <br />
      <label for="">Language Spoken</label> <br />
      <br />
      <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia</input><br />
      <input type="checkbox" name="language" value="English">English</input><br />
      <input type="checkbox" name="language" value="Other">Other</input><br />
      <br />
      <label for="">Bio</label> <br />
      <br />
      <textarea name="bio" cols="30" rows="10" placeholder="Biodata"></textarea><br />
      <input type="submit" value="Sign Up">
    </form>
    @endsection
