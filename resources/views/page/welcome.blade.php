@extends('layout.master')

@section('title')
Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{ucwords(strtolower($firstname))}} {{ucwords(strtolower($lastname))}}</h1>
    <h6>
      Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!
    </h6>
@endsection
